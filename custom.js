$(document).ready(function() {
    // Search
    $('ul.tert-nav li.searchit i').click(function(event) {
        if (event.target !== this) {
            return;
        }

        console.log("opening");
        $('li.searchit').addClass('search');
        $('.searchbox').fadeIn();
        $('ul.tert-nav li i').hide();
    });

    $('ul.tert-nav li.searchit img.closesearch').click(function(e) {
        e.stopPropagation();
        $('ul.tert-nav li i').fadeIn();
        $('.searchbox').hide();
        $('ul.tert-nav li').removeClass('search');
    });

    // toggle menu
    $('.toggle-menu').click(function() {
        $('.nav-menu').toggleClass('active');
    });

    // slide
    $(".slideshow > div:gt(0)").hide();

    setInterval(function() {
        $('.slideshow > div:first')
            // .fadeOut(2000)
            .next()
            .fadeIn(3000)
            .end()
            .appendTo('.slideshow');
    }, 3000);
})