<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Template</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="responsive.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="jquery.min.js"></script>
    <script src="custom.js"></script>
    <!-- <link href="https://fonts.googleapis.com/css?family=Oswald|Roboto:400,700&display=swap" rel="stylesheet"> -->
</head>

<body>
    <header>
        <div class="container">
            <div class="header-top">
                <div class="header-logo">
                    <a href="https://web-layout.herokuapp.com/">
                        <img src="image/logo.png" alt="logo">
                    </a>
                </div>
                <div class="header-contact">
                    <h2>PH (03) 9532 1357</h2>
                    <p>1 station st highett vic</p>
                </div>
            </div>
        
            <div class="header-menu">
                <div class="toggle-menu">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>
                <nav class="nav-menu">
                    <ul>
                        <li><a href="https://web-layout.herokuapp.com/"><i class="fa fa-home fa-2x" aria-hidden="true"></i></a></li>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Membership</a></li>
                        <li><a href="#">Restaurant</a></li>
                        <li><a href="#">Sports & Gaming</a></li>
                        <li><a href="#">Gallery</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </nav>
                <ul class="tert-nav">
                    <li class="searchit">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <div class="searchbox">
                            <img alt="" border="0" class="closesearch" src="image/icon-close.png" />
                            <input placeholder="search..." type="text" />
                            <!-- <input type="submit" value="" /> -->
                        </div>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </header>
    <section id="slide">
        <div class="container">
            <div class="slideshow">
                <div class="slide-bar">
                    <div class="slide-logo">
                        <img src="image/logo2.png" alt="logo2">
                    </div>
                    <img src="image/slide.jpg" alt="slide">
                    <div class="slide-content">
                        <h3>recently renovated</h3>
                        <h2>dining & bar areas</h2>
                        <a href="#">see more
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div class="slide-food">
                    <div class="slide-logo">
                        <img src="image/logo2.png" alt="logo2">
                    </div>
                    <img src="image/slide2.jpg" alt="slide food">
                    <div class="slide-content">           
                        <h2>fantastic food</h2>
                        <a href="#">see what's cooking
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="main">
        <section id="features">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <div class="item weekly">
                                    <a href="#">
                                        <img src="image/weekly.jpg" alt="weekly">
                                        <div class="item-content">
                                            <h2>Membership</h2>
                                            <p>Highett RSL Club Membership information</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="item sport">
                                    <a href="#">
                                        <img src="image/sport.jpg" alt="sport">
                                        <div class="item-content">
                                            <h2>Sporting groups</h2>
                                            <p>Footy tipping and sporting group information</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-6">
                                <div class="item cooking">
                                    <a href="#">
                                        <img src="image/cooking.jpg" alt="cooking">
                                        <div class="item-content">
                                            <h2>What's cooking</h2>
                                            <p>See our dining specials</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="item find-us">
                                            <a href="#">
                                                <img src="image/find-us.jpg" alt="find-us">
                                                <div class="item-content">
                                                    <h2>Find us</h2>
                                                    <p>1 STATION ST HIGHETT</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="item functions">
                                            <a href="#">
                                                <img src="image/functions.jpg" alt="functions">
                                                <div class="item-content">
                                                    <h2>Functions</h2>
                                                    <p>Coming soon...</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="content">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <div class="what-on">
                            <h2 class="content-title">What's on</h2>
                            <article class="item-news">
                                <img src="image/whats-on.png" alt="what's on">
                                <a class="news-title" href="#">Hunters & Collectors</a>
                                <h4>Saturday 30th july 2015</h4>
                                <p>Book in early to see the great Hunters & Colle...</p>
                                <a class="btn-readmore" href="#">Read more</a>
                            </article>
                            <article class="item-news">
                                <img src="image/whats-on.png" alt="what's on">
                                <a class="news-title" href="#">Hunters & Collectors</a>
                                <h4>Saturday 30th july 2015</h4>
                                <p>Book in early to see the great Hunters & Colle...</p>
                                <a class="btn-readmore" href="#">Read more</a>
                            </article>
                            <article class="item-news">
                                <img src="image/whats-on.png" alt="what's on">
                                <a class="news-title" href="#">Hunters & Collectors</a>
                                <h4>Saturday 30th july 2015</h4>
                                <p>Book in early to see the great Hunters & Colle...</p>
                                <a class="btn-readmore" href="#">Read more</a>
                            </article>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="about">
                            <h2 class="content-title">welcome to Highett RSL Club</h2>
                            <article class="content-about">
                                <p>
                                    Welcome to the Highett RSL Club, conveniently located at 1 Station St Highett (just south of the train station) in the heart of the Highett. Our newly renovated, modern, fully air conditioned club welcomes visitors to enjoy our facilities including our
                                    cafe, restaurant, TAB, gaming and club bar.
                                </p><br>
                                <p>We welcome our existing and new members, families and visitors to come and experience our new facilities.</p>
                                <div class="map" style="overflow:hidden;width: 100%;position: relative;">
                                    <iframe width="100%" height="251" src="https://maps.google.com/maps?width=100%&amp;height=251&amp;hl=en&amp;q=1%20Station%20St%20Highett+(Title)&amp;ie=UTF8&amp;t=&amp;z=10&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>

                                    <style>
                                        #gmap_canvas img {
                                            max-width: none!important;
                                            background: none!important
                                        }
                                    </style>
                                </div><br />
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-6 col-sm-12 col-xs-12">
                    <div class="col-4 col-sm-4 col-xs-6">
                        <div class="opening">
                            <h3 class="footer-title">CLUB OPENING HOURS</h3>
                            <ul>
                                <li>Mon - Thu: 10am - 4am</li>
                                <li>Fri: 10am - 6am</li>
                                <li>Sat: 9am - 6am</li>
                                <li>Sun: 9am - 4am</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-4 col-sm-4 col-xs-6">
                        <div class="contact-us">
                            <h3 class="footer-title">CONTACT US</h3>
                            <ul>
                                <li>Ph (03) 9532 1357</li>
                                <li>1 Station St Highett</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-4 col-sm-4 col-xs-12">
                        <div class="footer-logo">
                            <img src="image/logo3.png" alt="logo3">
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-12 col-xs-12">
                    <div class="footer-menu">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">What’s On</a></li>
                            <li><a href="#">Dining</a></li>
                            <li><a href="#">Facilites</a></li>
                            <li><a href="#">Functions</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="copyright">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li>
                                <p>© 2015 RSL Highett. All rights reserved. Designed by Easy Peas.</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>    
</body>

</html>